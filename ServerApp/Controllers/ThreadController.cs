﻿using ForumWebAppApi.DTOs;
using ForumWebAppApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ForumWebAppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThreadController : ControllerBase
    {
        IThreadService _threadService;

        public ThreadController(IThreadService threadService)
        {
            _threadService = threadService;
        }

        [HttpGet("{id}")]
        public ThreadPostsDto GetTopic(int id)
        {
            return _threadService.GetThreadPosts(id);
        }
    }
}