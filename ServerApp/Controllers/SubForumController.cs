﻿using ForumWebAppApi.DTOs;
using ForumWebAppApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ForumWebAppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubForumController : ControllerBase
    {
        ISubForumService _subForumService;

        public SubForumController(ISubForumService subForumservice)
        {
            _subForumService = subForumservice;
        }

        [HttpGet]
        public IEnumerable<SubForumTopicsDto> GetSubForums()
        {
            return _subForumService.GetSubForums();
        }
    }
}