﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ForumWebAppApi.DTOs;
using ForumWebAppApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ForumWebAppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TopicController : ControllerBase
    {
        private ITopicService _topicService;

        public TopicController(ITopicService topicService)
        {
            _topicService = topicService;
        }

        [HttpGet("{id}")]
        public TopicThreadsDto GetTopic(int id)
        {
            return _topicService.GetTopic(id);
        }
    }
}