﻿
using ForumWebAppApi.DTOs;
using ForumWebAppApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ForumWebAppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpPost]
        public void CreatePost(NewPostDto post)
        {
            this._postService.CreatePost(post);
        }
    }
}