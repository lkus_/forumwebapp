﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ForumWebAppApi.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SubForums",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubForums", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Topics",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    SubForumId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topics", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Topics_SubForums_SubForumId",
                        column: x => x.SubForumId,
                        principalTable: "SubForums",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Threads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TopicId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Threads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Threads_Topics_TopicId",
                        column: x => x.TopicId,
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Text = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    ThreadId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Threads_ThreadId",
                        column: x => x.ThreadId,
                        principalTable: "Threads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "SubForums",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Test SubForum 1" });

            migrationBuilder.InsertData(
                table: "SubForums",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Test SubForum 2" });

            migrationBuilder.InsertData(
                table: "SubForums",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "Test SubForum 3" });

            migrationBuilder.InsertData(
                table: "Topics",
                columns: new[] { "Id", "Description", "Name", "SubForumId" },
                values: new object[,]
                {
                    { 1, "Test description for topic 1", "Test Topic 1", 1 },
                    { 2, "Test description for topic 2", "Test Topic 2", 2 },
                    { 3, "Test description for topic 3", "Test Topic 3", 2 },
                    { 4, "Test description for topic 4", "Test Topic 4", 3 }
                });

            migrationBuilder.InsertData(
                table: "Threads",
                columns: new[] { "Id", "Description", "Name", "TopicId" },
                values: new object[,]
                {
                    { 1, "Test description for thread 1", "Test Thread 1", 1 },
                    { 2, "Test description for thread 2", "Test Thread 2", 1 },
                    { 3, "Test description for thread 3", "Test Thread 3", 2 },
                    { 4, "Test description for thread 4", "Test Thread 4", 3 },
                    { 5, "Test description for thread 5", "Test Thread 5", 3 },
                    { 6, "Test description for thread 1", "Test Thread 6", 4 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorName", "CreateDate", "Text", "ThreadId" },
                values: new object[,]
                {
                    { 1, "lukasz", new DateTime(2020, 2, 12, 12, 16, 54, 572, DateTimeKind.Local).AddTicks(9532), "Sample question", 1 },
                    { 2, "john", new DateTime(2020, 2, 13, 12, 16, 54, 575, DateTimeKind.Local).AddTicks(9049), "Sample answer", 1 },
                    { 3, "lukasz", new DateTime(2020, 2, 7, 12, 16, 54, 575, DateTimeKind.Local).AddTicks(9102), "Sample text", 2 },
                    { 4, "paul", new DateTime(2020, 2, 14, 12, 26, 54, 575, DateTimeKind.Local).AddTicks(9109), "Any discussion", 3 },
                    { 5, "lukasz", new DateTime(2020, 2, 14, 12, 24, 54, 575, DateTimeKind.Local).AddTicks(9137), "response", 3 },
                    { 6, "lukasz", new DateTime(2020, 2, 14, 10, 16, 54, 575, DateTimeKind.Local).AddTicks(9146), "Sample text", 4 },
                    { 10, "xyz", new DateTime(2020, 2, 12, 12, 16, 54, 575, DateTimeKind.Local).AddTicks(9182), "Sample text", 4 },
                    { 7, "tom", new DateTime(2020, 2, 10, 12, 16, 54, 575, DateTimeKind.Local).AddTicks(9165), "Sample text", 5 },
                    { 9, "john", new DateTime(2020, 2, 11, 12, 16, 54, 575, DateTimeKind.Local).AddTicks(9175), "Sample text", 5 },
                    { 8, "tal", new DateTime(2020, 2, 6, 12, 16, 54, 575, DateTimeKind.Local).AddTicks(9170), "Sample text", 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ThreadId",
                table: "Posts",
                column: "ThreadId");

            migrationBuilder.CreateIndex(
                name: "IX_Threads_TopicId",
                table: "Threads",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_Topics_SubForumId",
                table: "Topics",
                column: "SubForumId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Threads");

            migrationBuilder.DropTable(
                name: "Topics");

            migrationBuilder.DropTable(
                name: "SubForums");
        }
    }
}
