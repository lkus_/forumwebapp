﻿using ForumWebAppApi.DTOs;
using ForumWebAppApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Services.Interfaces
{
    public interface ITopicService
    {
        TopicThreadsDto GetTopic(int id);
    }
}
