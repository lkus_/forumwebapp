﻿using ForumWebAppApi.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Services.Interfaces
{
    public interface IPostService
    {
        void CreatePost(NewPostDto post);
    }
}
