﻿using ForumWebAppApi.DTOs;
using ForumWebAppApi.Models;
using System.Collections.Generic;

namespace ForumWebAppApi.Services.Interfaces
{
    public interface ISubForumService
    {
        IEnumerable<SubForumTopicsDto> GetSubForums();
        SubForumTopicsDto GetSubForum(int id);
    }
}
