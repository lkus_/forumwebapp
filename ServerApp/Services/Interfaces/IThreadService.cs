﻿using ForumWebAppApi.DTOs;
using ForumWebAppApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Services.Interfaces
{
    public interface IThreadService
    {
        ThreadPostsDto GetThreadPosts(int id);
        void CreateThread(int threadId, string title, string text);
    }
}
