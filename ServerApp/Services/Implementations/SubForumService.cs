﻿using ForumWebAppApi.Data;
using ForumWebAppApi.DTOs;
using ForumWebAppApi.Models;
using ForumWebAppApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Services.Implementations
{
    public class SubForumService : ISubForumService
    {
        private readonly ApplicationDbContext _context;

        public SubForumService(ApplicationDbContext context)
        {
            _context = context;
        }
        public SubForumTopicsDto GetSubForum(int id)
        {
            return _context.SubForums
                .Where(sf => sf.Id == id)
                .Select(sf => new SubForumTopicsDto()
                {
                    Id = sf.Id,
                    Name = sf.Name,
                    Topics = sf.Topics.Select(t => new TopicDto() { 
                        Id = t.Id,
                        Name = t.Name,
                        Description = t.Description,
                        ThreadsCount = t.Threads.Count
                    })
                })
                .FirstOrDefault();
        }

        public IEnumerable<SubForumTopicsDto> GetSubForums()
        {
            return _context.SubForums
                 .Select(sf => new SubForumTopicsDto()
                 {
                     Id = sf.Id,
                     Name = sf.Name,
                     Topics = sf.Topics.Select(t => new TopicDto()
                     {
                         Id = t.Id,
                         Name = t.Name,
                         Description = t.Description,
                         ThreadsCount = t.Threads.Count
                     })
                 })
                 .ToList();
        }
    }
}
