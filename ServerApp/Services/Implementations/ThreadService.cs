﻿using ForumWebAppApi.Data;
using ForumWebAppApi.DTOs;
using ForumWebAppApi.Models;
using ForumWebAppApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Services.Implementations
{
    public class ThreadService : IThreadService
    {
        private readonly ApplicationDbContext _context;

        public ThreadService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void CreateThread(int threadId, string title, string text)
        {
            //var newThread = new Thread()
            //{
            //    Name = 
            //}
        }

        public ThreadPostsDto GetThreadPosts(int id)
        {
            return _context.Threads
                .Where(t => t.Id == id)
                .Select(t => new ThreadPostsDto()
                {
                    Id = t.Id,
                    Name = t.Name,
                    Posts = t.Posts.Select(p => new PostDto() { 
                        Id = p.Id,
                        Text = p.Text,
                        AuthorName = p.AuthorName,
                        CreatedDate = p.CreateDate
                    })
                    .OrderBy(p => p.CreatedDate)
                })
                .FirstOrDefault();
        }
    }
}
