﻿using ForumWebAppApi.Data;
using ForumWebAppApi.DTOs;
using ForumWebAppApi.Models;
using ForumWebAppApi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Services.Implementations
{
    public class PostService : IPostService
    {
        private readonly ApplicationDbContext _context;

        public PostService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void CreatePost(NewPostDto post)
        {
            var newPost = new Post()
            {
                ThreadId = post.ThreadId,
                AuthorName = post.AuthorName,
                CreateDate = post.CreatedDate,
                Text = post.Text
            };

            _context.Posts.Add(newPost);
            _context.SaveChanges();
        }
    }
}
