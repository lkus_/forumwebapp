﻿using ForumWebAppApi.Data;
using ForumWebAppApi.DTOs;
using ForumWebAppApi.Models;
using ForumWebAppApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Services.Implementations
{
    public class TopicService : ITopicService
    {
        private readonly ApplicationDbContext _context;

        public TopicService(ApplicationDbContext context)
        {
            _context = context;
        }
        public TopicThreadsDto GetTopic(int id)
        {
            return _context.Topics
                .Where(t => t.Id == id)
                .Select(t => new TopicThreadsDto()
                {
                    Id = t.Id,
                    Name = t.Name,
                    Threads = t.Threads.Select(tt => new ThreadDto()
                    {
                        Id = tt.Id,
                        Name = tt.Name
                    })
                })
                .FirstOrDefault();
        }
    }
}
