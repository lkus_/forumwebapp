﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Models
{
    public class Topic : BaseEntity
    {
        public string Description { get; set; }
        public int SubForumId { get; set; }
        public virtual SubForum SubForum { get; set; }
        public virtual ICollection<Thread> Threads { get; set; }
    }
}
