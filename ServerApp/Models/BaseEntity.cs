﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Models
{
    abstract public class BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
