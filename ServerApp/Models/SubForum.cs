﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Models
{
    public class SubForum : BaseEntity
    {
        public virtual ICollection<Topic> Topics { get; set; }
    }
}
