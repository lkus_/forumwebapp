﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set; }
        public string AuthorName { get; set; }
        public int ThreadId { get; set; }
        public virtual Thread Thread { get; set; }
    }
}
