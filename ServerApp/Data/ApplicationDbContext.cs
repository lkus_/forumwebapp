﻿using ForumWebAppApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //    var testSubForum = new SubForum()
            //    {
            //        Id = 1,
            //        Name = "SubForum 1",
            //        Topics = new List<Topic>()
            //    };

            //    var topic = new Topic()
            //    {
            //        Id = 1,
            //        Name = "Topic 1",
            //        Description = "Topic 1 description",
            //        SubForum = testSubForum
            //    };

            //    var thread = new Thread()
            //    {
            //        Id = 1,
            //        Name = "Thread 1",
            //        Description = "Thread 1 description",
            //        Topic = topic
            //    };

            //    modelBuilder.Entity<SubForum>().HasData(testSubForum);
            //    modelBuilder.Entity<Topic>().HasData(topic);
            //    modelBuilder.Entity<Thread>().HasData(thread);
            //}

            //modelBuilder.Entity<SubForum>(mb =>
            //{
            //    mb.HasData(new SubForum
            //    {
            //        Id = 1,
            //        Name = "SubForum 1",
            //    });
            //    mb.OwnsMany(e => e.Topics).HasData(new List<Topic>() {
            //        new Topic()
            //        {
            //            Id = 1,
            //            Name = "Topic 1",
            //            Description = "Description topic 1"
            //        },
            //        new Topic()
            //        {
            //            Id = 2,
            //            Name = "Topic 2",
            //            Description = "Description topic 2"
            //        },
            //    });
            //});

            //modelBuilder.Entity<Topic>(mb =>
            //{
            //    mb.HasOne(r => r.SubForum);
            //});

            //modelBuilder.Entity<Thread>(mb =>
            //{
            //    mb.HasOne(r => r.Topic);
            //});

            modelBuilder.Entity<SubForum>()
                .HasMany(d => d.Topics)
                .WithOne(p => p.SubForum);

            modelBuilder.Entity<Topic>()
                .HasOne(d => d.SubForum)
                .WithMany(p => p.Topics)
                .HasForeignKey(d => d.SubForumId);

            modelBuilder.Entity<Thread>()
                .HasOne(d => d.Topic)
                .WithMany(p => p.Threads)
                .HasForeignKey(d => d.TopicId);

            modelBuilder.Entity<Post>()
                .HasOne(d => d.Thread)
                .WithMany(p => p.Posts)
                .HasForeignKey(d => d.ThreadId);



            var subForums = new List<SubForum>()
            {
                new SubForum
                    {
                        Id = 1,
                        Name = "Test SubForum 1"
                    },
                    new SubForum
                    {
                        Id = 2,
                        Name = "Test SubForum 2"
                    },
                    new SubForum
                    {
                        Id = 3,
                        Name = "Test SubForum 3"
                    }
            };

            var topics = new List<Topic>()
            {
               new Topic
                    {
                        Id = 1,
                        Name = "Test Topic 1",
                        Description = "Test description for topic 1",
                        SubForumId = 1
                    },
                    new Topic
                    {
                        Id = 2,
                        Name = "Test Topic 2",
                        Description = "Test description for topic 2",
                        SubForumId = 2
                    },
                    new Topic
                    {
                        Id = 3,
                        Name = "Test Topic 3",
                        Description = "Test description for topic 3",
                        SubForumId = 2
                    },
                    new Topic
                    {
                        Id = 4,
                        Name = "Test Topic 4",
                        Description = "Test description for topic 4",
                        SubForumId = 3
                    }
            };

            var threads = new List<Thread>()
            {
                new Thread
                    {
                        Id = 1,
                        Name = "Test Thread 1",
                        Description = "Test description for thread 1",
                        TopicId = 1
                    },
                    new Thread
                    {
                        Id = 2,
                        Name = "Test Thread 2",
                        Description = "Test description for thread 2",
                        TopicId = 1
                    },
                    new Thread
                    {
                        Id = 3,
                        Name = "Test Thread 3",
                        Description = "Test description for thread 3",
                        TopicId = 2
                    },
                    new Thread
                    {
                        Id = 4,
                        Name = "Test Thread 4",
                        Description = "Test description for thread 4",
                        TopicId = 3
                    },
                    new Thread
                    {
                        Id = 5,
                        Name = "Test Thread 5",
                        Description = "Test description for thread 5",
                        TopicId = 3
                    },
                    new Thread
                    {
                        Id = 6,
                        Name = "Test Thread 6",
                        Description = "Test description for thread 1",
                        TopicId = 4
                    }
            };

            var posts = new List<Post>()
            {
                new Post
                    {
                        Id = 1,
                        Text = "Sample question",
                        AuthorName = "lukasz",
                        CreateDate = DateTime.Now.AddDays(-2),
                        ThreadId = 1
                    },
                    new Post
                    {
                        Id = 2,
                        Text = "Sample answer",
                        AuthorName = "john",
                        CreateDate = DateTime.Now.AddDays(-1),
                        ThreadId = 1
                    },
                    new Post
                    {
                        Id = 3,
                        Text = "Sample text",
                        AuthorName = "lukasz",
                        CreateDate = DateTime.Now.AddDays(-7),
                        ThreadId = 2
                    },
                    new Post
                    {
                        Id = 4,
                        Text = "Any discussion",
                        AuthorName = "paul",
                        CreateDate = DateTime.Now.AddMinutes(10),
                        ThreadId = 3
                    },
                    new Post
                    {
                        Id = 5,
                        Text = "response",
                        AuthorName = "lukasz",
                        CreateDate = DateTime.Now.AddMinutes(8),
                        ThreadId = 3
                    },
                    new Post
                    {
                        Id = 6,
                        Text = "Sample text",
                        AuthorName = "lukasz",
                        CreateDate = DateTime.Now.AddHours(-2),
                        ThreadId = 4
                    },
                    new Post
                    {
                        Id = 7,
                        Text = "Sample text",
                        AuthorName = "tom",
                        CreateDate = DateTime.Now.AddDays(-4),
                        ThreadId = 5
                    },
                    new Post
                    {
                        Id = 8,
                        Text = "Sample text",
                        AuthorName = "tal",
                        CreateDate = DateTime.Now.AddDays(-8),
                        ThreadId = 6
                    },
                    new Post
                    {
                        Id = 9,
                        Text = "Sample text",
                        AuthorName = "john",
                        CreateDate = DateTime.Now.AddDays(-3),
                        ThreadId = 5
                    },
                    new Post
                    {
                        Id = 10,
                        Text = "Sample text",
                        AuthorName = "xyz",
                        CreateDate = DateTime.Now.AddDays(-2),
                        ThreadId = 4
                    }
            };

            modelBuilder.Entity<SubForum>().HasData(subForums);
            modelBuilder.Entity<Topic>().HasData(topics);
            modelBuilder.Entity<Thread>().HasData(threads);
            modelBuilder.Entity<Post>().HasData(posts);
        }

        public DbSet<SubForum> SubForums { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Thread> Threads { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
}