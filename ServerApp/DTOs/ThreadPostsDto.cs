﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.DTOs
{
    public class ThreadPostsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<PostDto> Posts { get; set; }
    }
}
