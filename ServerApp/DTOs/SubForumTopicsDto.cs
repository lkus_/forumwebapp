﻿using ForumWebAppApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.DTOs
{
    public class SubForumTopicsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<TopicDto> Topics { get; set; }
    }
}
