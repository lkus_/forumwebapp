﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumWebAppApi.DTOs
{
    public class TopicThreadsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<ThreadDto> Threads { get; set; }
    }
}
