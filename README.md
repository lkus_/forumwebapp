# How to run project

- Run **Update-Database** in Package manager console in Visual Studio
- Run Web Api solution
- Go to the ClientApp directory and run **ng serve --o**
