import { Component, Input } from '@angular/core';
import { Element } from 'src/app/models/element';

@Component({
  selector: 'app-sub-element',
  templateUrl: './sub-element.component.html',
  styleUrls: ['./sub-element.component.scss']
})
export class SubElementComponent {
    @Input() element: Element;
    @Input() elementType: string;
}