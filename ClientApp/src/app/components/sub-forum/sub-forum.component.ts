import { Component, Input} from '@angular/core';
import { Topic } from 'src/app/models/topic';

@Component({
  selector: 'app-sub-forum',
  templateUrl: './sub-forum.component.html',
  styleUrls: ['./sub-forum.component.scss']
})
export class SubForumComponent {
    @Input() name: string;
    @Input() topics: Topic[];
}