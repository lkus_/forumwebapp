import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NewPost } from '../models/newPost';

@Injectable()
export class PostService {
    private postApiUrl = environment.apiUrl + 'post';
    constructor(private http: HttpClient) { }

    createPost(post: NewPost): Observable<NewPost> {
        const body = JSON.stringify(post);
        return this.http.post<NewPost>(this.postApiUrl, body, {headers: {'Content-Type': 'application/json'}});
    }
}
