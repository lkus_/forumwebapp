import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Thread } from '../models/thread';
import { NewPost } from '../models/newPost';

@Injectable()
export class ThreadService {
    private threadApiUrl = environment.apiUrl + 'thread';
    constructor(private http: HttpClient) { }

    getThread(id: number): Observable<Thread> {
        return this.http.get<Thread>(this.threadApiUrl + '/' + id);
    }
}

  