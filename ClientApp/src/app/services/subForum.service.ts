import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SubForum } from '../models/subForum';

@Injectable()
export class SubForumService {
    private subForumApiUrl = environment.apiUrl + 'SubForum';
    constructor(private http: HttpClient) { }

    getSubForums(): Observable<SubForum> {
        return this.http.get<SubForum>(this.subForumApiUrl);
    }
}