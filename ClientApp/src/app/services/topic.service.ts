import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Topic } from '../models/topic';
import { environment } from 'src/environments/environment';

@Injectable()
export class TopicService {
    private topicApiUrl = environment.apiUrl + 'topic/';
    constructor(private http: HttpClient) { }

    getTopic(id: number): Observable<Topic> {
        return this.http.get<Topic>(this.topicApiUrl + id);
    }
}