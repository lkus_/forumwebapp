export class NewPost{
    threadId: number;
    authorName: string;
    createdDate: Date;
    text: string;
}