import { Element } from './element';
import { Post } from './post';


export class Thread extends Element {
    posts: Post[];
}
