import { Thread } from './thread';
import { Element } from './element';

export class Topic extends Element {
    threads: Thread[];
}