export class Post{
    id: number;
    text: string;
    authorName: string;
    createdDate: Date;
}