import { Topic } from './topic';

export class SubForum {
    id: number;
    name: string;
    topics: Topic[];
}