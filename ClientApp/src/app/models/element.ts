export abstract class Element {
    id: number;
    name: string;
    description: string;
}