import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { JoditAngularModule } from 'jodit-angular';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './containers/home/home.component';
import { SubForumComponent } from './components/sub-forum/sub-forum.component';
import { SubElementComponent } from './components/subElement/sub-element.component';
import { TopicComponent } from './containers/topic/topic.component';
import { ThreadComponent } from './containers/thread/thread.component';
import { PostComponent } from './components/post/post.component';

import { SubForumService } from './services/subForum.service';
import { PostService } from './services/post.service';
import { TopicService } from './services/topic.service';
import { ThreadService } from './services/thread.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ThreadComponent,
    SubElementComponent,
    SubForumComponent,
    TopicComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    JoditAngularModule,
    FormsModule
  ],
  providers: [
    SubForumService,
    TopicService,
    ThreadService,
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
