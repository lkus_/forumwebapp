import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './containers/home/home.component';
import { TopicComponent } from './containers/topic/topic.component';
import { ThreadComponent } from './containers/thread/thread.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'topic/:id', component: TopicComponent},
  { path: 'thread/:id', component: ThreadComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }