import { Component, OnInit } from "@angular/core";
import { Topic } from 'src/app/models/topic';
import { ActivatedRoute } from '@angular/router';
import { TopicService } from 'src/app/services/topic.service';

@Component({
    selector: 'app-topic',
    templateUrl: './topic.component.html',
    styleUrls: ['./topic.component.scss']
})

export class TopicComponent implements OnInit {
    id: string;
    topic: Topic;
    constructor(private route: ActivatedRoute, private topicService: TopicService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.id = params['id'];   
        });
        this.getTopic(this.id);
    }

    getTopic(id) {
        this.topicService.getTopic(id).subscribe(response => {
            this.topic = response;
        })   
    }
}