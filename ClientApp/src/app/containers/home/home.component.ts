import { Component } from "@angular/core";
import { SubForum } from 'src/app/models/subForum';
import { SubForumService } from 'src/app/services/subForum.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent {
    subForums: SubForum;
    constructor(private subForumService: SubForumService) { }
  
    ngOnInit() {
      this.getSubForums();
    }
  
    getSubForums() {
      this.subForumService.getSubForums().subscribe(response => {
        this.subForums = response;
      })
    }
}