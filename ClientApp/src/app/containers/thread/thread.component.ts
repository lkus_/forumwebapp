import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Thread } from 'src/app/models/thread';
import { NewPost } from 'src/app/models/newPost';
import { ThreadService } from 'src/app/services/thread.service';
import { PostService } from 'src/app/services/post.service';

@Component({
    selector: 'app-thread',
    templateUrl: './thread.component.html',
    styleUrls: ['./thread.component.scss']
})

export class ThreadComponent implements OnInit {
    richTextboxValue: string;
    id: string;
    thread: Thread;
    userName: string = '';

    constructor(private route: ActivatedRoute,
        private threadService: ThreadService,
        private postService: PostService
        ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.id = params['id'];   
        });
        this.getThread(this.id);
    }

    getThread(id) {
        this.threadService.getThread(id).subscribe(response => {
            this.thread = response;
        })   
    }

    editorValueChanged(editor) {
        this.richTextboxValue = editor.value;
      }

    addPost(){
        if(this.userName == ''){
            alert('User Name cannot be empty')
            return;
        }

        if(this.richTextboxValue == undefined){
            alert('Cannot send empty message')
            return;
        }

        const newPost: NewPost = {
            threadId: parseInt(this.id),
            text: this.richTextboxValue,
            authorName: this.userName,
            createdDate: new Date()
            
        };

        this.postService.createPost(newPost).subscribe(success=>{
            this.getThread(this.id);
            this.richTextboxValue = '';
        });
    }
}